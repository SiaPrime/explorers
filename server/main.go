package main

import (
	"fmt"
	"github.com/rs/cors"
	"gitlab.com/SiaPrime/explorers/server/router"
	"log"
	"net/http"
	"os"
)

// setupGlobalMiddleware will setup CORS
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	log.Println(handler)
	handleCORS := cors.Default().Handler
	return handleCORS(handler)
}

func main() {
	serverEnv := os.Getenv("SERVER_ENV")
	routerMux := router.NewRouter()
	if serverEnv == "PRODUCTION" {
		fmt.Println("Listening on port 3000")
		http.Handle("/", http.FileServer(http.Dir("../client/dist")))
		log.Fatal(http.ListenAndServe(":3001", setupGlobalMiddleware(routerMux)))
	} else {
		fmt.Println("Running in development mode, listening on port 3000")
		log.Fatal(http.ListenAndServe(":3001", setupGlobalMiddleware(routerMux)))
	}
}
