package siaclient

import (
	"encoding/json"
	"fmt"
	"net/http"
)
// TODO: use global json type of any for all daemon api response
var (
	consensusGet map[string] interface{}
	consensusBlocksGet map[string] interface{}
	// consensusValidateTxGet map[string] interface{}
)


func ConsensusStatusHandler(w http.ResponseWriter, r *http.Request) {
	apiErr := get("http://" + daemonUrl + "/consensus", &consensusGet)

	if apiErr != nil {
		fmt.Println("GetConsensus ERR: \n", apiErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(consensusGet)

	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}
}

func ConsensusBlocksHandler(w http.ResponseWriter, r *http.Request) {
	v := r.URL.Query()

	id := v.Get("id")
	height := v.Get("height")
	q := ""

	if id != "" {
		q = "id=" + id
	} else {
		q = "height=" + height
	}

	apiErr := get("http://" + daemonUrl + "/consensus/blocks?"+ q, &consensusBlocksGet)

	if apiErr != nil {
		fmt.Println("GetConsensusBlock ERR: \n", apiErr)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// return json
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	err := json.NewEncoder(w).Encode(consensusBlocksGet)

	if _, isJsonErr := err.(*json.SyntaxError); isJsonErr {
		panic("failed to return json")
	}


}