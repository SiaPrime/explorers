package siaclient

import (
	"gitlab.com/SiaPrime/explorers/server/types"
)

var SiaRoutes = types.Routes {
	{
		"GetAllHost",
		"GET",
		"/host/all",
		AllHostHandler,
	},
	{
		"GetActiveHost",
		"GET",
		"/host/active",
		ActiveHostHandler,
	},
	{
		"GetExplorerStatus",
		"GET",
		"/explorer",
		ExplorerStatusHandler,
	},
	{
		"GetExplorerBlocks",
		"GET",
		"/explorer/blocks/{height}",
		ExplorerBlocksHandler,
	},
	{
		"GetExplorerHash",
		"GET",
		"/explorer/hashes/{hash}",
		ExplorerHashHandler,
	},
	{
		"GetConsensus",
		"GET",
		"/consensus",
		ConsensusStatusHandler,
	},
	{
		"GetBlock",
		"GET",
		"/consensus/blocks",
		ConsensusBlocksHandler,
	},
}