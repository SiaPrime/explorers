import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { handleError } from '../share/error/service-error-handler';

@Injectable({
  providedIn: 'root'
})
export class HostService {

  constructor(private http: HttpClient) {
  }

  getAllHosts(): Observable<any> {
    return this.http
      .get('/v1/hostdb/all')
      .pipe(catchError(handleError));
  }

  getActiveHosts(): Observable<any> {
    return this.http
      .get('/v1/hostdb/active')
      .pipe(catchError(handleError));
  }

}

