import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { handleError } from '../share/error/service-error-handler';

export interface BlockAPIParams {
  id?: string;
  height?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConsensusService {
  api = '/v1/consensus';

  constructor(private http: HttpClient) {
  }

  getConsensus(): Observable<any> {
    return this.http
      .get(this.api)
      .pipe(catchError(handleError));
  }

  blocks(param: BlockAPIParams) {
    const q = param.id ? 'id' : 'height';
    const v = param.id || param.height;

    return this.http
      .get(`${this.api}/blocks`, {params: new HttpParams().set(q, v)})
      .pipe(catchError(handleError));
  }
}
