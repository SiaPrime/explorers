import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { handleError } from '../share/error/service-error-handler';


// @ts-ignore
@Injectable({
  providedIn: 'root'
})
export class ExplorerService {
  api = '/v1/explorer';
  constructor(private http: HttpClient) {
  }

  getExplorer(): Observable<any> {
    return this.http
      .get(this.api)
      .pipe(catchError(handleError));
  }

  getBlocks(height: string): Observable<any> {
    return this.http
      .get(`${this.api}/blocks/${height}`)
      .pipe(catchError(handleError));
  }

  getHash(hash: string): Observable<any> {
    return this.http
      .get(`${this.api}/hashes/${hash}`)
      .pipe(catchError(handleError));
  }

}
