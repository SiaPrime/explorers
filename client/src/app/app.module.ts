
import { NgModule } from '@angular/core';
import { reducers, metaReducers } from './reducers';
import { AppEffects } from './app.effects';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { HostEffects, ConsensusEffects } from './store';
import { OverviewModule } from './modules/overview/overview.module';
import { SharedModules } from './share/shared.module';
import { StoreModule } from '@ngrx/store';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OverviewModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot(
      [
        AppEffects,
        HostEffects,
        ConsensusEffects,
      ]),
    SharedModules
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
