import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { ConsensusService } from '../../services/consensus.service';
import { ActionTypes as actions, GetBlock } from './consensus.action';

@Injectable()
export class ConsensusEffects {
  constructor(private action$: Actions, private consensusService: ConsensusService) {
  }

  @Effect()
  loadConsensus$ = this.action$
    .pipe(
      ofType(actions.GetConsensus),
      mergeMap(() => this.consensusService.getConsensus()
        .pipe(
          map(consensus => ({type: actions.LoadConsensusResponse, payload: consensus})),
          catchError(() => EMPTY)
        ))
    );

  @Effect()
  loadBlock$ = this.action$
    .pipe(
      ofType(actions.GetBlock),
      mergeMap((action: GetBlock) => this.consensusService.blocks(action.payload)
        .pipe(
          map(block => ({type: actions.LoadBlockResponse, payload: block})),
          catchError(() => EMPTY)
        ))
    );
}
