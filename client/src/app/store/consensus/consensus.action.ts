import { Action } from '@ngrx/store';
import { BlockAPIParams } from '../../services/consensus.service';


export enum ActionTypes {
  GetConsensus = '[Consensus] Get Consensus',
  LoadConsensusResponse = '[Consensus API] Consensus Response Success',
  GetBlock = '[Consensus] Get Block',
  LoadBlockResponse = '[Consensus API] Block Loaded Success'
}

export class GetConsensus implements Action {
  readonly type = ActionTypes.GetConsensus;
}

export class GetBlock implements Action {
  readonly type = ActionTypes.GetBlock;

  constructor(public payload: BlockAPIParams) {
  }
}


// API Actions
export class GetConsensusSuccess implements Action {
  readonly type = ActionTypes.LoadConsensusResponse;

  constructor(public payload: any) {
  }
}

export class LoadBlockResponse implements Action {
  readonly type = ActionTypes.LoadBlockResponse;

  constructor(public payload: any) {
  }
}


export type Actions = GetConsensus | GetConsensusSuccess | GetBlock | LoadBlockResponse;
