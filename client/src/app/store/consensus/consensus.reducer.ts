import * as Consensus from './consensus.action';

export interface State {
  status: any;
  block: any;
}

const initialState: State = {
  status: null,
  block: null,
};

export function reducer(state = initialState, action: Consensus.Actions): State {
  switch (action.type) {
    case Consensus.ActionTypes.LoadConsensusResponse: {
      return {
        ...state,
        ...{status: action.payload}
      };
    }
    case Consensus.ActionTypes.LoadBlockResponse: {
      return {
        ...state,
        ...{block: action.payload}
      };
    }

    default: {
      return state;
    }

  }
}
