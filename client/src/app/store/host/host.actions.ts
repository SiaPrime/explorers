import { Action } from '@ngrx/store';

export enum ActionTypes {
  LoadAllHost = '[Host] Load All',
  LoadHostsSuccess = '[Host API] Hosts Loaded Success',
  ActiveHosts = '[Host] Active',
  OnlineHosts = '[Host] Online',
  LoadHostGigabytePerMonth = '[Host] Load Host Gigabyte Per Month',
  LoadHostGigabytePerMonthSuccess = '[Host API] Host GigabytePerMonth Load Success'
}


export class LoadAllHost implements Action {
  readonly type = ActionTypes.LoadAllHost;
}

export class ActiveHosts implements Action {
  readonly type = ActionTypes.ActiveHosts;
}

export class OnlineHosts implements Action {
  readonly type = ActionTypes.OnlineHosts;
}

export class LoadHostGigabytePerMonth implements Action {
  readonly type = ActionTypes.LoadHostGigabytePerMonth;
}

// API actions
export class LoadHostsSuccess implements Action {
  readonly type = ActionTypes.LoadHostsSuccess;
  constructor(public payload: any) {}
}

export class LoadHostGigabytePerMonthSuccess implements Action {
  readonly type = ActionTypes.LoadHostGigabytePerMonthSuccess;
  constructor(public payload: any) {}
}


export type Actions = LoadAllHost | ActiveHosts |
  OnlineHosts | LoadHostsSuccess | LoadHostGigabytePerMonth | LoadHostGigabytePerMonthSuccess;
