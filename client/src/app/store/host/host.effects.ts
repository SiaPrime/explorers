import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { catchError, map, mergeMap, take, tap } from 'rxjs/operators';
import { HostService } from '../../services/host.service';
import { ActionTypes as actions } from './host.actions';


// use take(1) if you want to fetch only once
// take(1) //to fetch the host only the first time the LoadAllHost action was dispatched.

@Injectable()
export class HostEffects {
  constructor(private actions$: Actions, private hostService: HostService)
  {}

  @Effect()
  loadHosts$ = this.actions$
    .pipe(
      ofType(actions.LoadAllHost),
      mergeMap(() => this.hostService.getAllHosts()
        .pipe(
          map(hosts => ({type: actions.LoadHostsSuccess, payload: hosts})),
          catchError(() => EMPTY)
        ))
    );

  @Effect()
  loadHostsInGigabyte = this.actions$
    .pipe(
      ofType(actions.LoadHostGigabytePerMonth),
      mergeMap(() => this.hostService.getAllHosts()
        .pipe(
          map(hosts => ({type: actions.LoadHostGigabytePerMonthSuccess, payload: hosts})),
          catchError(() => EMPTY)
        ))
    );

}

