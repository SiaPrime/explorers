import * as Host from './host.actions';
import * as byte from 'bytes';
import * as bigInt from "big-integer";

export interface State {
  all: Array<any>;
  active: Array<any>;
  online: Array<any>;
}

export interface NormalizedHostData {
  acceptingContracts: any;
  publicKey: any;
  netAddress: any;
  usedStorage: any;
  remainingStorage: any;
  totalStorage: any;
  contractPrice: any;
  storagePrice: any;
  uploadBandWidthPrice: any;
  downloadBandWidthPrice: any;
  collateralPrice: any;
  version: any;
  firstSeen: any;
  uptime: any;
  scoreBreakDown: any;
  scanHistory: Array<any>;
}

const initialState: State = {
  all: [],
  active: [],
  online: []
};

const BytesPerGigabyte: any = bigInt(1e9);
const BlockBytesPerMonthGigabyte: any = BytesPerGigabyte.times(4320);

const getBytes = bytes => {
  if (!bytes) {
    return null;
  }
  return byte(bytes, {unit: 'GB', decimalPlaces: 0}).match(/[0-9]*/)[0];
};

const hastingToSCP = hasting => {
  if (!hasting) {
    return null;
  }
  return (parseInt(hasting) / Math.pow(10, 24)).toFixed(2);
};

const getBandWidthPrice = hasting => {
  if (!hasting) {
    return null;
  }
  hasting = bigInt(hasting);
  return BytesPerGigabyte.times(hasting)
    .abs()
    .toJSNumber() / Math.pow(10, 24);
};

const mapHostStatus = (payload) => {
  const active: Array<any> = [];
  const online: Array<any> = [];

  payload.hosts.forEach(h => {
    if (h.acceptingcontracts) active.push(h);
    else (online.push(h));
  });

  return {active: active, online: online}
};

const normalizeHostData = (host): NormalizedHostData => {
  const storagePrice = (BlockBytesPerMonthGigabyte.times(bigInt(host.storageprice)))
    .abs()
    .toJSNumber() / Math.pow(10,24);
  const collateralPrice = (BlockBytesPerMonthGigabyte.times(bigInt(host.collateral)))
    .abs()
    .toJSNumber() / Math.pow(10, 24);


  return {
    acceptingContracts: host.acceptingcontracts,
    publicKey: host.publickey.key,
    netAddress: host.netaddress,
    usedStorage:  getBytes(host.totalstorage - host.remainingstorage),
    remainingStorage: getBytes(host.remainingstorage),
    totalStorage: getBytes(host.totalstorage),
    contractPrice: hastingToSCP(host.contractprice),
    storagePrice: storagePrice.toFixed(2),
    downloadBandWidthPrice: getBandWidthPrice(host.downloadbandwidthprice).toFixed(2),
    uploadBandWidthPrice: getBandWidthPrice(host.uploadbandwidthprice).toFixed(2),
    collateralPrice: collateralPrice.toFixed(2),
    version: host.version,
    scoreBreakDown: host.scorebreakdown,
    firstSeen: host.firstseen,
    uptime: host.historicuptime,
    scanHistory: host.scanhistory,
  };
};


export function reducer(state = initialState, action: Host.Actions): State {
  switch (action.type) {
    case Host.ActionTypes.LoadHostsSuccess: {
      const hostBuckets = mapHostStatus(action.payload);
      return {
        ...state,
        ...{all: action.payload.hosts, active: hostBuckets.active, online: hostBuckets.online }
      };
    }

    case Host.ActionTypes.LoadHostGigabytePerMonthSuccess: {
      const hostBuckets = mapHostStatus(action.payload);

      // perform price calculations
      const active = hostBuckets.active.map(h => normalizeHostData(h));
      const all = action.payload.hosts.map(h => normalizeHostData(h));
      const online = hostBuckets.online.map(h => normalizeHostData(h));
      console.log(active);

      return {
        ...state,
        ...{all: all, active: active, online: online }
      };
    }

    case Host.ActionTypes.ActiveHosts: {
      return {
        ...state,
        active: [],
      };
    }

    case Host.ActionTypes.OnlineHosts: {
      return {
        ...state,
        online: [],
      }
    }

    default: {
      return state;
    }
  }
}


