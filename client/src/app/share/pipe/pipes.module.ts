import { NgModule } from '@angular/core';
import {ByteToPipe} from './byte-to-gb.pipe';
import { HastingPipe } from './hasting.pipe';
import { RunFunctionPipe } from './run-function.pipe';

@NgModule({
  declarations: [ByteToPipe, HastingPipe, RunFunctionPipe],
  exports: [ByteToPipe, HastingPipe, RunFunctionPipe]
})
export class PipeModules {}
