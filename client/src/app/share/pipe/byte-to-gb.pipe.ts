import {Pipe, PipeTransform} from '@angular/core';
import * as byte from 'bytes';

@Pipe({name: 'byteTo'})
export class ByteToPipe implements PipeTransform {

  transform(bytes: number = 0, unit = 'GB'): string {
    return byte(bytes, {unit: unit, decimalPlaces: 0}).match(/[0-9]*/)[0];
  }
}
