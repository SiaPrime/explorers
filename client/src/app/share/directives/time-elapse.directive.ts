import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Renderer2
} from '@angular/core';
import * as moment from 'moment-timezone';
import { Moment } from 'moment';


@Directive({
  selector: '[spcTimeElapse]',
})
export class TimeElapseDirective implements OnInit, OnDestroy, OnChanges {
  @Input('spcTimeElapse') timeStamp: number | string;

  timer: any = null;
  localEl: any;
  elapsedEl: any;
  date: any | Moment;
  zone: any;

  constructor(
    private el: ElementRef,
    private r: Renderer2) {
  }

  ngOnInit(): void {
    this.zone = moment.tz.guess(true);
    this.localEl = this.r.createElement('p');
    this.r.appendChild(this.el.nativeElement, this.localEl);

    this.elapsedEl = this.r.createElement('p');
    this.r.appendChild(this.el.nativeElement, this.elapsedEl);

  }

  ngOnChanges(): void {
    if (!this.timeStamp) {
      return;
    }

    this.date = moment.unix(this.timeStamp);
    this.setLocalTime();
    this.setElapsedTime();
  }

  ngOnDestroy(): void {
    clearInterval(this.timer);
  }

  private setLocalTime(): void {
    const local = this.date.tz(this.zone).format('dddd, MMMM Do YYYY, H:mm:ss');
    this.r.setProperty(this.localEl, 'innerText', `TimeStamp (Local): ${local}`);
  }

  private setElapsedTime(): void {
    // clear timer if any to avoid mem leaks
    if (this.timer) {
      clearInterval(this.timer);
    }

    this.timer = setInterval(() => {
      const duration = moment.duration(moment().diff(this.date));
      const hours = `${!!duration.hours() ? `${duration.hours()}h ` : ''}`;
      const minutes = `${!!duration.minutes() ? `${duration.minutes()}m ` : ''}`;
      const seconds = `${!!duration.seconds() ? `${duration.seconds()}s ` : ''}`;

      this.r.setProperty(this.elapsedEl, 'innerText', `Time From Last Block: ${hours}${minutes}${seconds}`);

    }, 1000);
  }

}
