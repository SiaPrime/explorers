import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomMaterialModules } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipeModules } from './pipe/pipes.module';
import {TimeElapseDirective} from './directives/time-elapse.directive';


@NgModule({
  imports: [
  ],
  exports: [
    TimeElapseDirective
  ],
  declarations: [TimeElapseDirective],
})
export class DirectiveModule {}
