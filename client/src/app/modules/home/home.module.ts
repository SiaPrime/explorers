import {NgModule} from '@angular/core';
import {SharedModules} from '../../share/shared.module';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent,
  ],
  imports: [
    SharedModules,
    HomeRoutingModule
  ]
})
export class HomeModule {
}
