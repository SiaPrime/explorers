import { Component, OnInit } from '@angular/core';
import { ConsensusService } from '../../../services/consensus.service';
import { Store } from '@ngrx/store';
import { consensusActions } from '../../../store';
import { selectBlock, selectConsensusStatus } from '../../../reducers';
import { filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'spc-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  consensus$: any;
  block$: any;

  constructor(private cs: ConsensusService, private store: Store<any>) {
  }

  ngOnInit() {
    this.store.dispatch({type: consensusActions.GetConsensus});

    this.store.select(selectConsensusStatus)
      .pipe(filter(con => con !== null))
      .subscribe(con => {
        this.consensus$ = con;
        this.store.dispatch({type: consensusActions.GetBlock, payload: {height: con.height}});
      });

    this.store.select(selectBlock)
      .pipe(filter(block => block !== null))
      .subscribe(block => this.block$ = block);
  }

}
