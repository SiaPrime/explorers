import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ExplorerService } from '../../services/explorer.service';

@Component({
  selector: 'spc-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder, private es: ExplorerService) {
  }

  ngOnInit() {

    this.es.getExplorer()
      .subscribe(res => console.log('EXPLORER', res));

    this.form = this.fb.group({
      searchStr: [null, Validators.required]
    });
  }

  onSubmit(form) {
    console.log('FOmr', form.value);
  }

}
