import { NgModule } from '@angular/core';
import { OverviewComponent } from './overview.component';
import { SharedModules } from '../../share/shared.module';
import { HostDetailComponent } from './components/host-detail/host-detail.component';


@NgModule({
  declarations: [
    OverviewComponent,
    HostDetailComponent,
  ],
  imports: [
    SharedModules
  ]
})
export class OverviewModule { }
