import {Component, OnInit} from '@angular/core';
import {HostService} from '../../services/host.service';
import {Store} from '@ngrx/store';
import {consensusActions, hostActions} from '../../store';
import {selectConsensusStatus, selectHost} from '../../reducers';

import {parse} from 'json2csv';


interface Consensus {
  synced: boolean;
  height: number;
  currentBlock: string;
  target: Array<number>;
}

@Component({
  selector: 'spc-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  consensus$: any;
  hosts$: any;
  detailedView: any;
  // observe: Observable<any> = this.store.select(state => state.host)

  displayedColumns = [
    'Announced Address',
    'Used Storage',
    'Remaining Storage',
    'Total Storage',
    'Contract Price',
    'Storage Price per/GB/month',
    'Upload Bandwidth price per GB',
    'Download Bandwidth price per GB',
    'Offered Collateral per GB/month',
    'Running Version First seen (blockheight)',
    'Uptime (uptime/totaltime)',
    'Last Successful Scan',
  ];

  constructor(private hostService: HostService, private store: Store<any>) {
  }

  ngOnInit() {
    this.store.dispatch({type: hostActions.LoadHostGigabytePerMonth});
    this.store.dispatch({type: consensusActions.GetConsensus});

    this.store.select(selectHost)
      .subscribe(state => this.hosts$ = state);

    this.store.select(selectConsensusStatus)
      .subscribe(state => this.consensus$ = state);

  }

  // TODO: remove later once we move price calculation to server
  mostRecentScan(scans: Array<any>) {
    const mostRecent = scans.find(s => s.success === true);
    return mostRecent ? mostRecent.timestamp : null;
  }

  // export to CSV, move to server later
  exportToCSV() {
    const fields = [
      'netAddress', 'usedStorage', 'remainingStorage',
      'totalStorage', 'contractPrice', 'storagePrice', 'collateralPrice', 'uploadBandWidthPrice',
      'downloadBandWidthPrice'
    ];
    const tsfOpts = {encoding: 'utf-8'};
    const opts = {fields};

    try {
      const csv = parse(this.hosts$.all, opts, tsfOpts);
      const meta = `data:text/csv;charset=utf-8${csv} \n`;
      const encodedUri = encodeURI(meta);
      window.open(encodedUri);
    } catch (err) {
      console.log('CSV Export Err:', err);
    }
  }

}
