import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'spc-host-detail',
  templateUrl: 'host-detail.component.html',
  styleUrls: ['host-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HostDetailComponent implements OnChanges {
  @Input() host: any;
  @Output() closeView = new EventEmitter<any>();

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes.host);
    console.log(this.host);
  }


}
