import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer, select, Store
} from '@ngrx/store';
import {
  HostReducer, HostState,
  ConsensusReducer, ConsensusState
} from '../store';

import { environment } from '../../environments/environment';

export interface State {
  host: HostState;
  consensus: ConsensusState;
}

export const reducers: ActionReducerMap<State> = {
  host: HostReducer,
  consensus: ConsensusReducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

// Host Selectors
export const selectHost = (state: State) => state.host;
export const selectActiveHosts = createSelector(selectHost, (host: any) => host.active);


// Consensus Selectors
export const selectConsensus = (state: State) => state.consensus;
export const selectConsensusStatus = createSelector(selectConsensus, con => con.status);
export const selectBlock = createSelector(selectConsensus, con => con.block);
