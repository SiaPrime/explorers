# Network-Overview

This tool is to display `hostdb` statistics from SiaPrime.

## Implementation

Currently, it is a stand-alone web-app that connects directly to `spd`. In the future, we will properly have a server to handle requests
from this web-client. The tool is its own module in an angular application, so we can move it anywhere we want. Right now, we
just need a simple tool for our work.

Notes
* this is for internal usage only, there are security flaws from accessing `spd` directly from a web-client 
* Must use FireFox browser, because we need to set `user-agent`, and FF is the only browser as of today that allows it


## Development Environment
**Server**
We're running a golang web server, you'll need to install gin for live-reload
cd server \
gin run main

\
**Client** \
cd client \
Install `node` and `npm install -g @angular/cli`. \
Run `npm i && ng serve` on first pull. \
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
